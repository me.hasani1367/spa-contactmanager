<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:api')->group(function (){

    Route::get('/contacts',[App\Http\Controllers\ContactsController::class,'index']);
    Route::post('/contacts',[App\Http\Controllers\ContactsController::class,'store']);
    Route::get('/contacts/{contact}', [App\Http\Controllers\ContactsController::class,'show']);
    Route::patch('/contacts/{contact}', [App\Http\Controllers\ContactsController::class,'update']);
    Route::delete('/contacts/{contact}', [App\Http\Controllers\ContactsController::class,'destroy']);
});

